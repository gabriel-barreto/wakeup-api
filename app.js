// ==> Dependencies
const fetch = require("node-fetch");
const schedule = require("node-schedule");

// ==> Getting APIs hosts
const APIS = require("./apis.json");

// ==> Configs
global.fetch = fetch;

const run = endpoint => {
    fetch(endpoint)
        .then(() => console.log(`${endpoint}... DONE!`))
        .catch(err => {
            console.log(err);
        });
};

const main = () => {
    APIS.map(each => {
        schedule.scheduleJob("WakeUpAPI", "10 15,45 * * * *", () => {
            run(each);
        });
        console.log(`${each}... SCHEDULED!`);
    });
};

// ==> Starting
main();
