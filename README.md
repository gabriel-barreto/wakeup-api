# WakeUp API

A package to keep alive your API hosted in heroku free dyno

_In free dynos Heroku shutdown all applications after 30 minutos of inativity_

## Requirements

Consulting **engines** props in **[package.json](https://bitbucket.org/gabriel-barreto/wakeup-api/src/master/package.json)** file.

**I'm using**

-   node v10.9.0
-   npm v6.2.0

## Running

### 1. Clone this repo

```shell
$ git clone git@bitbucket.org/gabriel-barreto/wakeup-api.git
```

### 2. Install all project dependencies

```shell
$ npm install
```

### 3. Update your APIs list

Modify the **[apis.json](https://bitbucket.org/gabriel-barreto/wakeup-api/src/master/apis.json)** file and put into that all your apis address

### 4. Run

```shell
$ npm start
```

_Your endpoint will be requested every 15 and 45 minutes to keep your api alive_

### 5. Modify running time

```javascript
schedule.scheduleJob("WakeUpAPI", **PUT YOUR TIME HERE**, () => {
    run(each);
});
```

_Consulting **[node-schedule](https://www.npmjs.com/package/node-schedule)** docs to learn how modify that time_

**Remember that:**

Before modify any file, please, restart the repo

```shell
$ rm -Rf .git
$ git init
```

## License

[MIT](https://bitbucket.org/gabriel-barreto/wakeup-api/master/LICENSE.md)

## Contributing

-   Clone this repo
-   Create your feature branch
-   **DO SOMETHING GREAT!!!**
-   Submit a pull request
-   Enjoy
